## To run any code or notebooks, assuming you have anaconda for python 3 installed, you can run the following commands at a mac command line to get set up. First navigate to the folder containg these notebooks, then run:


$ conda create -y -n wine-copy-env python=3.5

$ source activate wine-copy-env

$ python -m pip install jupyter

$ ipython kernel install --user --name=wine-copy-project

$ pip install -r requirements.txt

$ conda install openpyxl

$ jupyter notebook

Jupyter notebooks should then open in your default browser. Open the notebook, select 'Change Kernel' from the 'Kernel' dropdown menu, and select the wine-copy-project kernel.

You should then be able to step through all the cells.
